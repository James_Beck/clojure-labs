(ns assignment1-2018.core
  (:gen-class))

(require '[clojure.string :as strEdit])

(defn count-occurrences
  ([]
   "To utilise this method, please enter a hashmap; first is a key; the second is the occurrences of that key in a vector")
  ([key-quantityVector-hashmap]
   (map #(count (second %)) key-quantityVector-hashmap)))

(defn create-multiset
  ([]
   "To utilise this method, please enter a list of string data")
  ([listed-items]
   (let [key-quantityVector-hashmap (group-by identity listed-items)]
     (zipmap (keys key-quantityVector-hashmap ) (count-occurrences key-quantityVector-hashmap)))))

(defn size-multiset
  ([]
   "To utilise this method, please enter a list of string data")
  ([listed-items]
   (reduce + (vals listed-items))))

(defn intersect-multisets
  ([]
   "To utilise this method, please enter two hash-maps")
  ([multiset1 multiset2]   
   (select-keys (merge-with min multiset1 multiset2)
     (sort (clojure.set/intersection (set (keys multiset1)) (set (keys multiset2)))))))

(defn create-multiset-from-text-file
  ([]
   "To utilise this method, please reference a text")
  ([text-file]
   (create-multiset (strEdit/split-lines text-file))))

(defn breaklines-from-spaces
  ([]
   "To utilise this method, please input a string")
  ([text-body]
   (strEdit/replace text-body #" " "\n")))

(defn analyse-sentiment
  ([]
   "To utilise this method, please input three multisets of data"
   "The first will be compared to the third, as will the second"
   "The output will be the overall correlation, where a negative values means multiset 2 and 3 correlate more than multiset 1 and 3")
  ([multiset1 multiset2 file-import]
   (let [multiset3 (create-multiset-from-text-file (breaklines-from-spaces (slurp file-import)))
         positive-value (size-multiset (intersect-multisets multiset1 multiset3))
         negative-value (size-multiset (intersect-multisets multiset2 multiset3))
         final-comparison (- positive-value negative-value)]
     (println "file" (last (strEdit/split file-import #"/")))
     (println "positive score " positive-value)
     (println "negative score " negative-value)
     (println "final sentiment " final-comparison))))

(defn part1
  []
  (let [multiset1 (create-multiset '("ab" "cd" "ef" "ab" "fg" "ef" "ab" "dd"))
        multiset2 (create-multiset '("zz" "ef" "fg" "ef" "ab" "ab" "ef" "ef"))
        intersection-of-multisets (intersect-multisets multiset1 multiset2)]
    (println "multiset1 = " multiset1 ", size = " (size-multiset multiset1))
    (println "multiset2 = " multiset2 ", size = " (size-multiset multiset2))
    (println "multiset intersection = " intersection-of-multisets ", size = " (size-multiset intersection-of-multisets))))

(defn part2
  []
  (let [positive-words (create-multiset-from-text-file (slurp "unix-text-files/positive-words.txt"))
        negative-words (create-multiset-from-text-file (slurp "unix-text-files/negative-words.txt"))]
    (println "size of positive dictionary = " (size-multiset positive-words))
    (println "size of negative dictionary = " (size-multiset negative-words))
    (analyse-sentiment positive-words negative-words "unix-text-files/pos1.txt")
    (analyse-sentiment positive-words negative-words "unix-text-files/pos2.txt")
    (analyse-sentiment positive-words negative-words "unix-text-files/pos3.txt")
    (analyse-sentiment positive-words negative-words "unix-text-files/neg1.txt")
    (analyse-sentiment positive-words negative-words "unix-text-files/neg2.txt")
    (analyse-sentiment positive-words negative-words "unix-text-files/neg3.txt")))
    
  
  



