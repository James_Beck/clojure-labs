(ns lab3.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args])

(defn create-matrix
  [rows cols]
  ;Returns a matrix of zeros with size (rows cols)
  (vec
    (repeat rows
      (vec 
        (repeat cols 0)))))
;; What does this do?
;; I want to repeat the number 0 by the number of columns
;; I want to store that within a vector
;; I want to repeat this vector by the number of rows
;; Then I want to store those vectors within a vector

(create-matrix 4 3)

(defn get-dims-matrix
  [matrix]
  ; Returns a hashmap containing the dimensions of the matrix
  (hash-map 
    :rows (count matrix) 
    :cols (count (first matrix))))
;; What does this do?
;; I want to have a count of the number of rows
;; I want to have a count of the number of columns
;; I want to return the previous two values in a hashmap

(get-dims-matrix (create-matrix 4 3))

(defn get-matrix
  [matrix row col]
  ;Returns the element of the matrix at position (row, col)
  (nth (nth matrix row) col))
;; What does this do?
;; I want to get the element in a matrix -- pretty much it

(get-matrix (create-matrix 4 3) 0 2)

(defn set-matrix
  [matrix row col val]
  ;Returns a copy of matrix with the element at (row, col) set to the given value
  (assoc matrix row
    (assoc (get-matrix-row matrix row) col val)))
;; What does this do?
;; We take in a matrix
;; Get the position of a value by finding the row and col
;; Change the value at this position to the value input

;; This is how I see it breaking down.
;; We are going to change the value of one of the vectors
;; (assoc matrix row "hi") would change the value of the entire vector to hi [[0 0 0][0 0 0] "hi" [0 0 0]]
;; Therefore we need to modify a single vector of interest, which we will then replace with a current vector
;; To select a specific vector in the vector of vectors, we use (nth matrix row)
;; What we have now is the vector that represents a single row
;; We will then modify one of the values in this row (assoc (nth matrix row) col val)
;; This means to take a vector that is our row of interest. Associate that with a column, and replace with the val
;; Next we want to replace one of the vectors in our vector of vectors
;; To do this we use (assoc matrix row (our edited vector))
  
(set-matrix (create-matrix 4 3) 3 2 "hi")

(defn get-matrix-row
  [matrix row]
  ;Returns a vector containing all matrix elements from the specified row
  (nth matrix row))
;; We've done this before

(get-matrix-row (create-matrix 4 3) 3)

(defn get-matrix-col
  [matrix col]
  ; Returns a vector containing all matrix elements from the specified column
  (let [rows (count matrix)]
    (vec 
      (for [x (range rows)] (nth (nth matrix x) col)))))
    
;; What does this do? -- This one is a little complicated
;; The first thing we do is set a variable for how many rows are in the array
;; The next thing we do is create a range of values which we'll use to iterate the selection of a row
;; We can capture a single row by getting the (nth matrix **our current row) -- where row is the position of the row in our matrix
;; After capturing this row, we then want to select the col
;; We do this in a very similar way (nth (row vector selected) col) -- where col is position of the column
;; We want to iterate over all values of rows, so we create a for loop, applying the range rows to the value x
;; This will x the value to be each of the rows, and we can apply it to select each row, and therefore each column
;; Finally we are left with a list values that represent a column, we then want to convert these into a vector to output

(get-matrix-col (create-matrix 4 3) 0)

(defn query-matrix
  [matrix query-val]
  ;Returns a vector specifiying the position of the first matching query-val in the matrix, or nil if query-val does not exist
  (let [array-info (get-dims-matrix matrix)
        response (.indexOf (vec (flatten matrix)) query-val)]
    (if (> response -1) 
      (do 
        [(int (/ response (get array-info :rows))) (mod response (get array-info :rows))]) 
      nil)))
;; What does this do? -- Again this one is a little complicated
;; The first thing we want to do is be able to find the index of the number in the matrix
;; When it isn't found in the matrix, then we want to declare 'nil'
;; To do this, we flatten the matrix, then we convert it into a vector
;; We then want to search this vector for the index of our querying value
;; For this we use the .indexOf class which is a java class
;; Clojure has one called get, but it doesn't work with vectors
;; Now we know if the query-val is within the array, and if so; where it's position is in the flattened vector
;; Now we want to figure out where in the matrix that value was
;; To figure out in which row it is in, we can take our response and divide it by the amount of rows
;; To figure out which column it is in, we can look for the excess; by taking the modules of response and row length
;; This will give us how far into the row it's been 'pushed' or the column in which it lies.
;; We then output these two values in a vector

(query-matrix [[0 1 2][3 4 5][6 7 8]] 8)
