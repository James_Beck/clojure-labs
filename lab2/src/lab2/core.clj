(ns lab2.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

; My solution to question 1
(defn even-numbers-divide-by-seven
  [range-min range-max]
  (def entire-range (apply hash-set (range range-min range-max)))
  (def entire-range-mod-14 (apply hash-set (range 0 range-max 14)))
  (sort (clojure.set/intersection entire-range entire-range-mod-14)))

; Mike's solution to question 1
(hash-set 
  (subvec
    (filterv
      even? 
      (range 0 200 7))
    8))
;or
(use 'clojure.set)
(sort (intersection
        (set (filter even?
               (range 100 201)))
        (set (filter #(= 0 (mod % 7))
               (range 100 201)))))

; My solution to question 2
; This is a terrible solution but I was struggling to apply keyword to the string coverted elements in the range 97-123
(defn ascii-hash-map
  []
  (def num-range (sort (apply hash-set (range 97 123))))
  (def hash-key [:a :b :c :d :e :f :g :h :i :j :k :l :m :n :o :p :q :r :s :t :u :v :w :x :y :z])
  (interleave hash-key num-range))

; Mike's solution to question 2
(let [ascii-codes (range 97 123)]
  (apply hash-map
    (interleave (map #(keyword (str (char %)))
                  ascii-codes)
      ascii-codes)))

; My solution to question 3
(def tabular-data [{:x 1 :y 2}{:x 2 :y 3}{:x 24 :y 10}])
(defn sum-of-xs
  ([]
   (reduce + (map :x tabular-data)))
  ([custom-data]
   (reduce + (map :x custom-data))))

; Mike's solution to question 3
;; We got the exact same method for this

; My solution to question 4
(defn print-strings-norm
  [& strings]
  (println (str (vec strings))))

(defn print-strings-doseq
  [& strings]
  (doseq [e strings]
    (println e)))
  
(defn print-strings-recur
  ([strings]
   (if nil? strings
     (do (println (first strings))
       (recur (rest strings))))))

; Mike's solution to question 4
;; We had the same methodology for doseq
(defn print-strings2
  [& some-strings]
  (when-not (empty? some-strings)
    (do (println (first some-strings))
      (recur (rest some-strings)))))

; My solution to question 5


; Mike's solution to question 5
(defn count-single-char
  [string ch]
  (count
    (filter #(= % ch) string)))

(defn count-char1
  [string]
  {:a (count-single-char string \a)
   :c (count-single-char string \c)
   :g (count-single-char string \g)
   :t (count-single-char string \t)})
;; the recursive solution
(defn count-char2
  ([string]
   (count-char2 string {:a 0 :c 0 :g 0 :t 0}))
  ; Recursive version which takes a partial remaining string and 
  ; the hash-map of counts, and updates one of the counts by one
  ([remaining-string counts-so-far]
   (if (empty? remaining-string)
     ; returns the hash-map of counts
     counts-so-far
     ; otherwise convert the current character to a keyword
     ; and update the hash-map of counts to iterate
     (let [current-key (keyword (str (first remaining-string)))
           current-count (get counts-so-far current-key)]
       (recur (rest remaining-string)
         (assoc counts-so-far current-key (+ current-count 1)))))))

; My solution to question 6


; Mike's solution to question 6
(defn my-reverse
  ([my-list]
   (my-reverse my-list '()))
  ([remaining-list reversed-list]
   (if (empty? remaining-list)
     reversed-list
     (recur (rest remaining-list)
       (conj reversed-list (first remaining-list))))))
