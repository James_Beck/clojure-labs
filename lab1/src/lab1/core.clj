(ns lab1.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello to this new programming language, Clojure!"))

; Turns out this is just normal addition
(defn adding-fractions
  [frac1 frac2]
  (println (+ frac1 frac2)))

; This just normal equality
(defn equality-of-fractions
  [frac1 frac2]
  (println (= frac1 frac2)))
